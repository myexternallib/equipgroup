#!/bin/bash
# Configure the slave to replicate from the master
mysql -u"root" -p"root" <<EOF
  CHANGE MASTER TO
    MASTER_HOST='172.18.0.2',
    MASTER_PORT=3306,
    MASTER_USER='replica',
    MASTER_PASSWORD='replica',
    MASTER_LOG_FILE='mysql-bin.000003',
    MASTER_LOG_POS=157,
    GET_MASTER_PUBLIC_KEY=1;
  START SLAVE;
EOF
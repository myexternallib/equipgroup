#!/bin/bash

# Create a replication user on the master
mysql -h"localhost" -P"3306" -u"root" -p"root" <<EOF
  CREATE USER 'replica'@'172.18.0.3' IDENTIFIED BY 'replica';
  GRANT REPLICATION SLAVE ON *.* TO 'replica'@'172.18.0.3';
EOF